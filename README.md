# generator-cheshire

## Installation

First, install [Yeoman](http://yeoman.io) and generator-cheshire using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g https://gitlab.com/project-files/cheshire
npm install -g yo
npm install -g https://gitlab.com/generator/generator-cheshire.git
```

Then generate your new project:

```bash
yo cheshire # [--force] to initialize pages
cheshire  ~/Gitlab/your/theme/dist --yes # to generate the website
hs -c=1 -o ./public # local preview
```
## License
MIT © [Hominus Nocturna](http://hominusnocturna.com/)
