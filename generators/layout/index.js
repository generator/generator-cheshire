'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var _ = require('lodash');

module.exports = yeoman.Base.extend({


  constructor: function () {
    yeoman.Base.apply(this, arguments);
    this.option('source', { type: String, required: true, defaults: "default"});
    this.option('file',   { type: String, required: true, defaults: "default"});
  },

 

  writing: function () {
    this.fs.copyTpl(
      this.templatePath(this.options.source + '.html.ejs'),
      this.destinationPath('layout/'+_.kebabCase(this.options.file)+'.html'),
      {}
    );
  },


});
