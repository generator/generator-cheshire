'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var _ = require('lodash');

module.exports = yeoman.Base.extend({

  prompting: function () {
    var done = this.async();
    // Have Yeoman greet the user.
    this.log(yosay( 'Welcome to the cat\'s meow ' + chalk.red('generator-cheshire') + ' generator!' ));
    done();
  },

  writing: function () {
    // MERGE FOLDER
    this.fs.copy( this.templatePath('favicon.ico'), this.destinationPath('merge/favicon.ico'));

    // Some Content
    this.composeWith('cheshire:content', { options: {source:"menu", title:"Menu", file:"menu"}, args: [] });
    this.composeWith('cheshire:content', { options: {source:"footer", title:"Sitemap", file:"footer"}, args: [] });

    this.composeWith('cheshire:content', { options: {source:"default", title:"Home", file:"home"}, args:[] });
    this.composeWith('cheshire:content', { options: {source:"default", title:"News", file:"news"}, args:[] });
    this.composeWith('cheshire:content', { options: {source:"default", title:"Overview", file:"overview"}, args:[] });
    this.composeWith('cheshire:content', { options: {source:"default", title:"License", file:"license"}, args:[] });
    this.composeWith('cheshire:content', { options: {source:"default", title:"FAQ", file:"faq"}, args:[] });
    this.composeWith('cheshire:content', { options: {source:"default", title:"Links", file:"links"}, args:[] });
    this.composeWith('cheshire:content', { options: {source:"default", title:"Blog", file:"blog"}, args:[] });
    this.composeWith('cheshire:content', { options: {source:"default", title:"About", file:"about"}, args:[] });
    this.composeWith('cheshire:content', { options: {source:"default", title:"Contact", file:"contact"}, args:[] });

    // Default Layouts
    this.composeWith('cheshire:layout',  { options: {source:"default", file:"intro"}, args: [ ] });
    this.composeWith('cheshire:layout',  { options: {source:"default", file:"outer"}, args: [ ] });
    this.composeWith('cheshire:layout',  { options: {source:"default", file:"inner"}, args: [ ] });
    // A content page
    this.composeWith('cheshire:page',    { options: {source:"default", file:"index", title:"All Systems Nominal", content:"about@alpha blog@alpha home@beta license@beta links@beta news@gamma overview@gamma contact@delta faq@delta"}, args: [] });
  },

  install: function () {
    this.installDependencies();
  },

});
