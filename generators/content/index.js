'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var _ = require('lodash');

module.exports = yeoman.Base.extend({


  constructor: function () {
    yeoman.Base.apply(this, arguments);
    this.option('source', { type: String, required: true, defaults: "default"});
    this.option('file',   { type: String, required: true, defaults: "readme"});
    this.option('title',  { type: String, required: true, defaults: "Untitled"});
  },
 

  writing: function () {
    this.fs.copyTpl(
      this.templatePath(this.options.source + '.md.ejs'),
      this.destinationPath('content/'+_.kebabCase(this.options.file)+'.md'),
      { title: this.options.title }
    );
  },

});
