'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var _ = require('lodash');

module.exports = yeoman.Base.extend({

  constructor: function () {

    yeoman.Base.apply(this, arguments);

    this.option('source', { type: String, required: true, defaults: "default"});

    this.option('file',   { type: String, required: true, defaults: "blank"});

    this.option('layout', { type: String, required: true, defaults: "intro"});
    this.option('content', { type: String, required: true, defaults: "intro@main"});


    this.option('title', { type: String, required: true, defaults: "Untitled"});
    this.option('published', { type: Boolean, required: true, defaults: true});
    this.option('author', { type: String, required: true, defaults: "Administrator"});
    this.option('date', { type: String, required: true, defaults: new Date});

  },


  writing: function () {

    this.options.content = JSON.stringify( this.options.content.split(" ") );

    this.fs.copyTpl(
      this.templatePath(this.options.source + '.json.ejs'),
      this.destinationPath('pages/'+_.kebabCase(this.options.file)+'.json'),
      _.assignIn({}, this.options)
    );
  },


});
